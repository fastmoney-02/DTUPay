[![Build Status](http://fastmoney-02.compute.dtu.dk:8282/buildStatus/icon?job=DTUPay)](http://fastmoney-02.compute.dtu.dk:8282/job/DTUPay/)
# DTU PAY


## Features:
* Obtaining unique tokens
* Pay at Merchant which includes transfering money from customer account to merchant account using the tokens
* Managing customer and merchant accounts with DTUPay
* A reporting interface generates for a _customer_ the list of his transactions
* A reporting interface generates for a _merchant_ the list of his transactions
* Refund payment

## Links:
[Scrum Board](https://trello.com/b/Tl8RHYBY/dtu-pay)