package dtupay_customerservice.unittest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import dtupay_customerservice.CustomerDatabase;
import dtupay_customerservice.CustomerManager;
import dtupay_customerservice.interfaces.ICustomerEventProducer;
import dtupay_customerservice.models.Customer;
import models.AccountId;
import models.UserId;


import static org.mockito.Mockito.*;


public class CustomerManagerTest {

	private CustomerManager manager;
	private ICustomerEventProducer store;
	
	@Before
	public void setup() {
		store = mock(ICustomerEventProducer.class);
		manager = new CustomerManager(CustomerDatabase.getInstance(), store);
	}
	
	@Test
	public void createCustomer() throws Exception {
		Customer customer = manager.addCustomer(new AccountId("123"));
		assertNotNull(customer);
	}
	
	@Test
	public void createDebitorCallbackCalled() throws Exception {
		Customer customer = manager.addCustomer(new AccountId("123"));
		verify(store, times(1)).registerAccount(customer);

		assertNotNull(customer);
	}
	
	@Test
	public void customerIdsAreUnique() throws Exception {
		Customer customer1 = manager.addCustomer(new AccountId("123"));
		Customer customer2 = manager.addCustomer(new AccountId("123"));
		
		assertNotEquals(customer1, customer2);
	}
	
	@Test
	public void getExistingCustomer() throws Exception {
		Customer customer = manager.addCustomer(new AccountId("123"));
		customer = manager.getCustomer(customer.getId());
		assertNotNull(customer);
	}
	
	@Test
	public void getNonExistingCustomer() {
		Customer customer = manager.getCustomer(new UserId(""));
		assertNull(customer);
	}
	
	@Test
	public void deleteExistingCustomer() throws Exception {
		Customer customer = manager.addCustomer(new AccountId("123"));
		
		manager.deleteCustomer(customer.getId());
		
		verify(store, times(1)).deleteAccount(customer.getId());
		assertNull(manager.getCustomer(customer.getId()));
	}
	
	@Test
	public void deleteNonExistingCustomer() throws Exception {
		boolean result = manager.deleteCustomer(new UserId("99"));
		verify(store, times(0)).deleteAccount(any());

		assertFalse(result);
	}
}
