package dtupay_customerservice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import dataTransferObject.CustomerDTO;
import dtupay_customerservice.interfaces.ICustomerEventProducer;
import dtupay_customerservice.models.Customer;
import models.UserId;


/**
 * The Class CustomerEventConnector.
 */
public class CustomerEventConnector implements ICustomerEventProducer {

	/** The Constant EXCHANGE_NAME. */
	private static final String EXCHANGE_NAME = "events";
	
	/** The Constant AMQP_URL. */
	private static final String AMQP_URL = "amqp://guest:guest@fastmoney-02.compute.dtu.dk:5672";
	
	/** The object writer. */
	private ObjectWriter objectWriter;
	
	/** The event channel. */
	private Channel eventChannel;
	
	/**
	 * Instantiates a new customer event connector.
	 */
	public CustomerEventConnector()
	{
		objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		try {
			eventChannel = GetExchangeChannel();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	/* (non-Javadoc)
	 * @see _02.DTUPay_CustomerService.ICustomerEventProducer#registerAccount(models.Customer)
	 */
	public void registerAccount(Customer customer) throws Exception { 
			String routingKey = "Customer.Created";
			CustomerDTO customerDTO = new CustomerDTO(customer.getId().getValue(), customer.getAccountId().getValue());
			String message = objectWriter.writeValueAsString(customerDTO);
			
			publish(routingKey, message);
	}

	/* (non-Javadoc)
	 * @see _02.DTUPay_CustomerService.ICustomerEventProducer#deleteAccount(models.UserId)
	 */
	@Override
	public void deleteAccount(UserId userId) throws Exception {
			String routingKey = "Customer.Deleted";
			String message = objectWriter.writeValueAsString(userId.getValue());
			
			publish(routingKey, message);
	}    	

	/**
	 * Publish.
	 *
	 * @param topic the topic
	 * @param message the message
	 * @throws UnsupportedEncodingException the unsupported encoding exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void publish(String topic, String message) throws UnsupportedEncodingException, IOException
	{
		eventChannel.basicPublish(EXCHANGE_NAME, topic, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"));
		System.out.println(" [x] Sent '" + message + "' on " + topic);
	}
	
	/**
	 * Gets the exchange channel.
	 *
	 * @return the channel
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TimeoutException the timeout exception
	 * @throws KeyManagementException the key management exception
	 * @throws NoSuchAlgorithmException the no such algorithm exception
	 * @throws URISyntaxException the URI syntax exception
	 */
	private Channel GetExchangeChannel() throws IOException, TimeoutException, KeyManagementException, NoSuchAlgorithmException, URISyntaxException
	{		
		ConnectionFactory factory = new ConnectionFactory();
		factory.setUri(AMQP_URL);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(EXCHANGE_NAME, "topic");
		return channel;
	}
}
