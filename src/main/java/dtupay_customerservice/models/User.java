package dtupay_customerservice.models;

import models.AccountId;
import models.UserId;

/**
 * The Class User.
 */
public abstract class User {

	/** The id. */
	private UserId id;
	
	/** The account id. */
	private AccountId accountId;
	
	/**
	 * Instantiates a new user.
	 */
	public User() {}

	/**
	 * Instantiates a new user.
	 *
	 * @param id the id
	 * @param accountId the account id
	 */
	public User(UserId id, AccountId accountId) {
		this.id = id;
		this.accountId = accountId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public UserId getId() {
		return id;
	}
	
	/**
	 * Gets the account id.
	 *
	 * @return the account id
	 */
	public AccountId getAccountId() {
		return accountId;
	}
}
