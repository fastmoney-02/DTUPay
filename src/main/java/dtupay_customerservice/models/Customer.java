package dtupay_customerservice.models;

import java.util.UUID;

import models.AccountId;
import models.UserId;

/**
 * The Class Customer.
 */
public class Customer extends User {

	/**
	 * Instantiates a new customer.
	 */
	public Customer() {
		super();
	}
	
	/**
	 * Instantiates a new customer.
	 *
	 * @param id the id
	 * @param accountId the account id
	 */
	public Customer(UserId id, AccountId accountId) {
		super(id, accountId);
	}
	
	/**
	 * Instantiates a new customer.
	 *
	 * @param accountId the account id
	 */
	public Customer(AccountId accountId) {
		super(new UserId(UUID.randomUUID().toString()), accountId);
	}
	
	
}
