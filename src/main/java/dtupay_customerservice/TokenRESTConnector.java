package dtupay_customerservice;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import dataTransferObject.RequestTokensArgs;
import dtupay_customerservice.interfaces.ITokenService;

/**
 * The Class TokenRESTConnector.
 */
public class TokenRESTConnector implements ITokenService {

	/** The c. */
	Client c = ClientBuilder.newClient();
	
	/** The target. */
	WebTarget target;
	
	/**
	 * Instantiates a new token REST connector.
	 *
	 * @param target the target
	 */
	public TokenRESTConnector(String target) {
		super();
		this.target = c.target(target);
	}
	

	/* (non-Javadoc)
	 * @see interfaces.ITokenService#requestTokens(dataTransferObject.RequestTokensArgs)
	 */
	@Override
	public String[] requestTokens(RequestTokensArgs args) throws Exception {
		Response response = target
				.path("token")
				.request()
				.post(Entity.json(args));
		
		if(response.getStatusInfo().getFamily().equals(Status.Family.SERVER_ERROR)) {
			return new String[0];
		}
		return response.readEntity(String[].class);
	}

}
