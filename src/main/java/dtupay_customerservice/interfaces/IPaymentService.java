package dtupay_customerservice.interfaces;

import dataTransferObject.TransactionDTO;

/**
 * The Interface IPaymentService.
 */
public interface IPaymentService {
	
	/**
	 * Gets the customer transaction history.
	 *
	 * @param customerId the id of the customer
	 * @return the customer transaction history
	 */
	TransactionDTO[] getCustomerTransactionHistory(String customerId);

}
