package dtupay_customerservice.interfaces;

import javax.ws.rs.core.Response;

import dataTransferObject.CustomerDTO;
import dataTransferObject.RequestTokensArgs;
import dataTransferObject.TransactionDTO;

/**
 * The Interface ICustomerService.
 */
public interface ICustomerService {

	/**
	 * Register customer.
	 *
	 * @param accountId the new customers account id
	 * @return Http response
	 * @throws Exception
	 */
	Response registerCustomer(String accountId) throws Exception;

	/**
	 * Delete customer.
	 *
	 * @param id the user id of the customer wished to delete
	 * @return true, if successful
	 * @throws Exception
	 */
	boolean deleteCustomer(String id) throws Exception;

	/**
	 * Returns the customer.
	 *
	 * @param customerId the id of the wanted customer
	 * @return the wanted customer
	 */
	CustomerDTO getCustomer(String customerId);

	/**
	 * Gets the customer transaction history.
	 *
	 * @param customerId the if of the customer that wants it's transaction history
	 * @return the customers transaction history
	 */
	TransactionDTO[] getCustomerTransactionHistory(String customerId);

	/**
	 * Request tokens.
	 *
	 * @param args containing the user id and amount
	 * @return the tokenid array
	 * @throws Exception
	 */
	String[] requestTokens(RequestTokensArgs args) throws Exception;

}