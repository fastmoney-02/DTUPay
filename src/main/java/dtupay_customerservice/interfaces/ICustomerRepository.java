package dtupay_customerservice.interfaces;

import dtupay_customerservice.models.Customer;
import models.UserId;

/**
 * The Interface ICustomerRepository.
 */
public interface ICustomerRepository {
	
	/**
	 * Gets the customer.
	 *
	 * @param id of the wanted customer
	 * @return the customer found
	 */
	Customer getCustomer(UserId id);
	
	/**
	 * Adds the customer.
	 *
	 * @param customer that should be added to the database
	 * @return true, if successful
	 */
	boolean addCustomer(Customer customer);
	
	/**
	 * Delete customer.
	 *
	 * @param id of the customer who should be deleted
	 * @return true, if successful
	 */
	boolean deleteCustomer(UserId id);
}