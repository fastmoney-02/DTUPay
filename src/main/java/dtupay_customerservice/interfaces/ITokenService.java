package dtupay_customerservice.interfaces;

import dataTransferObject.RequestTokensArgs;

/**
 * The Interface ITokenService.
 */
public interface ITokenService {
	
	/**
	 * Request tokens.
	 *
	 * @param args containing the user id and amount
	 * @return the tokenid array
	 * @throws Exception
	 */
	public String[] requestTokens(RequestTokensArgs args) throws Exception;
	
	
}
