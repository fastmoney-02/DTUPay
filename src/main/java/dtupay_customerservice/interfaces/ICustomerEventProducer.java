package dtupay_customerservice.interfaces;

import dtupay_customerservice.models.Customer;
import models.UserId;

/**
 * The Interface ICustomerEventProducer.
 */
public interface ICustomerEventProducer {
	
	/**
	 * Register account.
	 *
	 * @param customer the customer that should be registered
	 * @throws Exception
	 */
	void registerAccount(Customer customer) throws Exception;
	
	/**
	 * Delete account.
	 *
	 * @param userId the id of the user who should be deleted
	 * @throws Exception
	 */
	void deleteAccount(UserId userId) throws Exception;
}
