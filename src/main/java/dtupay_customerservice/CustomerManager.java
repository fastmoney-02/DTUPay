
package dtupay_customerservice;

import dtupay_customerservice.interfaces.ICustomerEventProducer;
import dtupay_customerservice.interfaces.ICustomerRepository;
import dtupay_customerservice.models.Customer;
import models.AccountId;
import models.UserId;

/**
 * The Class CustomerManager.
 */
public class CustomerManager {
	
	/** The database. */
	private ICustomerRepository database;
	
	/** The customer event producer. */
	private ICustomerEventProducer customerEventProducer;

	
	/**
	 * Instantiates a new customer manager.
	 *
	 * @param database the database
	 * @param paymentService the payment service
	 */
	public CustomerManager(ICustomerRepository database, ICustomerEventProducer paymentService){
		this.database = database;
		this.customerEventProducer = paymentService;
	}
	
	/**
	 * Finds a customer.
	 *
	 * @param id of the wanted customer
	 * @return the found customer
	 */
	public Customer getCustomer(UserId id){
		return database.getCustomer(id);
	}
	
	/**
	 * Adds the customer.
	 *
	 * @param accountId the accountId of the customer who should be created
	 * @return the newly created customer
	 * @throws Exception
	 */
	public Customer addCustomer(AccountId accountId) throws Exception{
		Customer customer = new Customer(accountId);
		database.addCustomer(customer);
		try {
			customerEventProducer.registerAccount(customer);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		return customer;
	}
	
	/**
	 * Delete customer.
	 *
	 * @param id of the customer
	 * @return true, if successful
	 * @throws Exception
	 */
	public boolean deleteCustomer(UserId id) throws Exception{
		if(database.deleteCustomer(id))
		{
			customerEventProducer.deleteAccount(id);
			return true;
		}
			
		return false;
	}
}
