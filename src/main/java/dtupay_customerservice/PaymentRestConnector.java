package dtupay_customerservice;

import javax.ws.rs.core.MediaType;

import dataTransferObject.TransactionDTO;
import dtupay_customerservice.interfaces.IPaymentService;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;


/**
 * The Class PaymentRestConnector.
 */
public class PaymentRestConnector implements IPaymentService {

	/** The c. */
	Client c = ClientBuilder.newClient();
	
	/** The target. */
	WebTarget target;
	
	/**
	 * Instantiates a new payment rest connector.
	 *
	 * @param targeturl the targeturl
	 */
	public PaymentRestConnector(String targeturl) {
		super();
		this.target = c.target(targeturl);
	}

	/* (non-Javadoc)
	 * @see interfaces.IPaymentService#getCustomerTransactionHistory(java.lang.String)
	 */
	@Override
	public TransactionDTO[] getCustomerTransactionHistory(String customerId) {

		TransactionDTO[] response = target
				.path("transactions")
				.path("customer")
				.path(customerId)
				.request(MediaType.APPLICATION_JSON)
				.get(TransactionDTO[].class);
		return response;
	}

}
