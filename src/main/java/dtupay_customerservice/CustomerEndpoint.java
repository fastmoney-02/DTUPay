package dtupay_customerservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import dataTransferObject.CustomerDTO;
import dataTransferObject.RequestTokensArgs;
import dataTransferObject.TransactionDTO;
import dtupay_customerservice.interfaces.ICustomerEventProducer;
import dtupay_customerservice.interfaces.ICustomerRepository;
import dtupay_customerservice.interfaces.ICustomerService;
import dtupay_customerservice.interfaces.IPaymentService;
import dtupay_customerservice.interfaces.ITokenService;
import dtupay_customerservice.models.Customer;
import models.AccountId;
import models.UserId;

/**
 * The Class CustomerEndpoint.
 */
@Path("/customers")
public class CustomerEndpoint implements ICustomerService {
	
	/** The customer repository. */
	ICustomerRepository customerRepository = CustomerDatabase.getInstance();
	
	/** The token service. */
	ITokenService tokenService = new TokenRESTConnector("http://dtupay-tokenservice:8080/");
	
	/** The payment service. */
	IPaymentService paymentService = new PaymentRestConnector("http://dtupay-paymentservice:8080/");
	
	/** The customer event producer. */
	ICustomerEventProducer customerEventProducer = new CustomerEventConnector();

	/** The customer manager. */
	CustomerManager customerManager = new CustomerManager(customerRepository, customerEventProducer);
	
	/* (non-Javadoc)
	 * @see interfaces.ICustomerService#registerCustomer(java.lang.String)
	 */
	@Override
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerCustomer(String accountId) throws Exception {
		Customer customer = customerManager.addCustomer(new AccountId(accountId));
		CustomerDTO customerDTO = new CustomerDTO(customer.getId().getValue(), customer.getAccountId().getValue());
		ResponseBuilder resp = Response.status(201).entity(customerDTO);
		return resp.build(); 
    }
	
	
	/* (non-Javadoc)
	 * @see interfaces.ICustomerService#deleteCustomer(java.lang.String)
	 */
	@Override
	@DELETE
	@Path("{customerId}")
	public boolean deleteCustomer(@PathParam("customerId") String customerId) throws Exception{
		boolean success = customerManager.deleteCustomer(new UserId(customerId));
		if (!success) {
			throw new NotFoundException();
		}
		return success;
	}

	/* (non-Javadoc)
	 * @see interfaces.ICustomerService#getCustomer(java.lang.String)
	 */
	@Override
	@GET
	@Path("{customerId}")
	@Produces(MediaType.APPLICATION_JSON)
	public CustomerDTO getCustomer(@PathParam("customerId") String customerId) {
		Customer customer = customerManager.getCustomer(new UserId(customerId));
		
		if (customer == null) {
			throw new NotFoundException();
		}
		CustomerDTO customerDTO = new CustomerDTO(customer.getId().getValue(), customer.getAccountId().getValue());
		return customerDTO;
	}
	
	/* (non-Javadoc)
	 * @see interfaces.ICustomerService#getCustomerTransactionHistory(java.lang.String)
	 */
	@Override
	@GET
	@Path("{customerId}/transactions")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TransactionDTO[] getCustomerTransactionHistory(@PathParam("customerId") String customerId) {
		return paymentService.getCustomerTransactionHistory(customerId);
	}
	
	/* (non-Javadoc)
	 * @see interfaces.ICustomerService#requestTokens(dataTransferObject.RequestTokensArgs)
	 */
	@Override
	@POST
	@Path("requestTokens")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String[] requestTokens(RequestTokensArgs args) throws Exception { 
		return tokenService.requestTokens(args);
	}


}
