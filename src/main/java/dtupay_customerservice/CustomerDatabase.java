package dtupay_customerservice;

import java.util.ArrayList;

import dtupay_customerservice.interfaces.ICustomerRepository;
import dtupay_customerservice.models.Customer;
import models.UserId;

/**
 * The Class CustomerDatabase.
 */
public class CustomerDatabase implements ICustomerRepository {

	/** The database. */
	private static CustomerDatabase database = null;
	
	/** The customers. */
	ArrayList<Customer> customers;
	
	/**
	 * Instantiates a new customer database.
	 */
	private CustomerDatabase() {
		customers = new ArrayList<Customer>();
	}
	
	/**
	 * Gets the single instance of CustomerDatabase.
	 *
	 * @return single instance of CustomerDatabase
	 */
	public static CustomerDatabase getInstance() {
		if (database == null) {
			database = new CustomerDatabase();
		}
		return database;
	}
	
	
	/* (non-Javadoc)
	 * @see interfaces.ICustomerRepository#getCustomer(models.UserId)
	 */
	public Customer getCustomer(UserId id) {
		return customers.stream().filter(c -> c.getId().getValue().equals(id.getValue())).findFirst().orElse(null);
	}
	
	/* (non-Javadoc)
	 * @see interfaces.ICustomerRepository#addCustomer(models.Customer)
	 */
	public boolean addCustomer(Customer customer) {
		return customers.add(customer);
	}
	
	/* (non-Javadoc)
	 * @see interfaces.ICustomerRepository#deleteCustomer(models.UserId)
	 */
	public boolean deleteCustomer(UserId id) {
		return customers.removeIf(c -> c.getId().equals(id));
	}

}
